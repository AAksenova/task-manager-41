package ru.t1.aksenova.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.enumerated.TaskSort;
import ru.t1.aksenova.tm.dto.model.TaskDTO;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface ITaskService {

    @NotNull
    TaskDTO add(@Nullable TaskDTO task);

    @NotNull
    TaskDTO add(@Nullable String userId, @Nullable TaskDTO task);

    @NotNull
    @SneakyThrows
    Collection<TaskDTO> set(@NotNull Collection<TaskDTO> tasks);

    @NotNull
    TaskDTO create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    List<TaskDTO> findAll();

    @NotNull
    List<TaskDTO> findAll(@Nullable String userId);

    @NotNull
    List<TaskDTO> findAll(@Nullable String userId, @Nullable TaskSort sort);

    @Nullable
    TaskDTO findOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    TaskDTO findOneByIndex(@Nullable String userId, @Nullable Integer index);


    void clear();

    void removeAll(@Nullable String userId);

    @Nullable
    TaskDTO remove(@Nullable String userId, @Nullable TaskDTO task);

    @Nullable
    TaskDTO removeOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    TaskDTO removeOneByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    TaskDTO updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @SneakyThrows
    boolean existsById(@Nullable String userId, @Nullable String id);

    @SneakyThrows
    long getSize(@Nullable String userId);

    @NotNull
    TaskDTO updateProjectIdById(@Nullable String userId, @Nullable String id, @Nullable String project_id);

    @NotNull
    TaskDTO updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

    @NotNull
    TaskDTO changeTaskStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    TaskDTO changeTaskStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    @NotNull
    List<TaskDTO> findAll(@Nullable String userId, @Nullable Comparator<TaskDTO> comparator);

    @NotNull
    List<TaskDTO> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

}
